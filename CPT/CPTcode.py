"""
GPA Caculator
It asks how many assignment types there are, and for each class it asks the percent grades and the wieght of each section/assignment.
Then it gets the GPA of the classes.
"""

grades_mapping = {9:4, 8:3, 7:2, 6:1, 5:0, 4:0, 3:0, 2:0, 1:0}
#setup
all_grades = []
all_weights = []
assignmentType = []
all_class_grades = []
secret = False
#functions

#Takes in grades and weights
def CalculateAverage(grades, weights):
  average = 0
  sum_weights = 0
  
  for i in range(len(grades)):
    average += grades[i]*(weights[i]/100)
    sum_weights += weights[i]/100
    
  return average/sum_weights

#Takes in one grade, returns gpa
def getGPA(grade):
    if grade == 100: return 4
    if grade < 10.0: return 0
    if ((grade % 10) >= 8) and ((grade % 100) - (grade % 10) != 9):
      return grades_mapping[((grade % 100) - (grade % 10))/10]+0.5
    else:
      return grades_mapping[((grade % 100) - (grade % 10))/10]
    
#main
while True:
  question = input("Would you like us to calculate your GPA? Yes, or No? ")

  if question.lower() == "yes" or question.lower() == "y":
    pass
  else:
    print("\nOk, have an exceptional day :)")
    break
    
  num_o_cls = int(input("\nHow many classes do you have? "))

  #repeat for each class
  for i in range(num_o_cls):
    #reset
    all_grades = []
    all_weights = []

    assignmentType = input("What are the types of graded assignments in your class? Seperate your answer by a comma and a space. ")
    assignmentType = assignmentType.split(", ")
    for j in range(len(assignmentType)):
      all_weights.append(float(input("what is the weight percent of " + assignmentType[j] + "? ")))
      all_grades.append(float(input("what is your grade in " + assignmentType[j] + "? ")))

    all_class_grades.append(CalculateAverage(all_grades, all_weights))
  one_hundreds = []
  for i in range(len(all_class_grades)):
    one_hundreds.append(100)
  Final_grade = CalculateAverage(all_class_grades, one_hundreds)

  GPA = getGPA(Final_grade)
  print(f'\nYour GPA is {GPA} with a grade of {Final_grade}%\n')
