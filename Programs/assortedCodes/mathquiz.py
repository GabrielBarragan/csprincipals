from random import randint
num1 = randint(1, 10)
num2 = randint(1, 10)
tryAgain = "yes"
while (tryAgain == "yes" or  "Yes"):

    strikes = 1
    print("This is the Math Quiz. Three strikes and you're out, but you can try again")
    while (strikes != 3):
        question1 = int(input(f"What is {num1} times {num2}? "))
        while (question1 != num1 * num2):
            print(f"Strike {strikes}. Try again: ")
            strikes = strikes + 1
            question1 = int(input(f"What is {num1} times {num2}? "))
        print("Good job, question 1 complete!")
        num1 = randint(5, 10)
        num2 = randint(1, 5)
        question2 = int(input(f"What is {num1} minus {num2}? "))
        while (question2 != num1 - num2):
            print(f"Strike {strikes}. Try again: ")
            strikes = strikes + 1
            question2 = int(input(f"What is {num1}  minus {num2}? "))
            print("Good job, question 2 complete! ")
    tryAgain = input("Try again? ")
