from gasp import *
import random
class Player:
    pass

class Robot:
    pass

def place_player():
    global player, robot, robots
    player = Player()
    player.x = random.randint(0,63)
    player.y = random.randint(0,47)
    while robot.x == player.x and robot.y == player.y:
        player.x = random.randint(0,63)
        player.y = random.randint(0,47)
    player.shape = Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)

def place_robots():
    global robots, robot
    robots = []
    while len(robots) < numbots:
        robot = Robot()
        robot.x = random.randint(0,63)
        robot.y = random.randint(0,47)
        if not check_collisions(robot, robots):
            robot.shape = Box((10 * robot.x, 10 * robot.y), 10, 10, filled=True, thickness=1)
            robots.append(robot)

def move_player():
    global player
    key = update_when('key_pressed')
    if (key == 'q'):
        player.x -= 1
        player.y += 1
    elif (key == 'w'):
        player.y += 1
    elif (key == 'e'):
        player.x += 1
        player.y += 1
    elif (key == 'd'):
        player.x += 1
    elif (key == 'c'):
        player.x += 1
        player.y -= 1
    elif (key == 'x'):
        player.y -= 1
    elif (key == 'z'):
        player.x -= 1
        player.y -= 1
    elif (key == 'a'):
        player.x -=1
    elif(key == 's'):
        player.x = random.randint(0,63)
        player.y = random.randint(0,48)
    move_to(player.shape, (10 * player.x + 5, 10 * player.y + 5))

def move_robots():
    global robot, player
    if robot.x < player.x and robot.y < player.y:
        robot.x +=1
        robot.y +=1
    elif robot.x > player.x and robot.y < player.y:
        robot.x -=1
        robot.y +=1
    elif robot.x < player.x and robot.y > player.y:
        robot.x +=1
        robot.y -=1
    elif robot.x > player.x and robot.y > player.y:
        robot.x -=1
        robot.y -=1
    elif robot.x < player.x:
        robot.x +=1
    elif robot.x > player.x:
        robot.x -=1
    elif robot.y < player.y:
        robot.y +=1
    elif robot.y > player.y:
        robot.y -=1
    update_when('key_pressed')
    move_to(robot.shape, (10 * robot.x, 10 * robot.y))

def check_collisions(player, robots):
    for rob in robots:
        if player.x == rob.x and player.y == rob.y:
            return True
        return False


global finished
begin_graphics()
numbots = 10
finished = False
place_robots()
place_player()

while not finished:
    move_player()
    # move_robots()
    finished = check_collisions(player, robots)

Text("You've been caught!", (200, 100), size=10)
sleep(3)
end_graphics()
