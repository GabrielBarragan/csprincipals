def function1(x,y):
    """
    >>> function1(1, 5)
    67.5
    >>> function1(2,9)
    181462.0
    """
    sum1 = 0
    product1 = 1
    for i in range(x, y+1):
        sum1 += i
        product1 *= i
    avg = (sum1 + product1) / 2
    return avg)





if __name__ == "__main__":

    import doctest
    doctest.testmod()
