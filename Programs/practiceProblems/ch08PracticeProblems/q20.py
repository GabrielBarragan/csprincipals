def triangle(len):
    star = "*"
    space = " "
    print(star)
    for i in range(len):
        print(star + space + star)
        space = space + " "
    star2 = "* "
    lenMinus4 = len - 4
    if (len > 4):
        print(star2 * (len - lenMinus4))
    elif (len < 4):
        print(star2 * (len + 1))
    else:
        print(star2 * len)
lenSide = int(input("Enter the length of the side: "))
triangle(lenSide)
