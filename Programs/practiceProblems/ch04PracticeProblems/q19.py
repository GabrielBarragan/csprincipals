allSavings = 200
dollarsPerWeek = 20
dollarsPerMonth = dollarsPerWeek * 4
numMonths = allSavings / dollarsPerMonth
print(f"It will take {numMonths} months to earn {allSavings} if you make {dollarsPerWeek} dollars a week.")

