miles_per_hour = 70
miles_traveled = 140
hours_taken = miles_traveled / miles_per_hour
print("A car travelling at " + str(miles_per_hour) + " mph takes " + str(hours_taken) + " hours to go " + str(miles_traveled) + " miles." )

