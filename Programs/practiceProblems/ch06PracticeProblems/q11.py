def costTrip(x,y):
    num_gallons = x / y
    price_per_gallon = 3.45
    total = num_gallons * price_per_gallon
    return total
    
miles = 500
miles_per_gallon = 26
print(costTrip(miles, miles_per_gallon))
