number = int(input("Enter a number: "))
def who_knows_why(num):
    sum = 0
    product = 1
    list1 = range(0, num+1, 2)
    for i in list1:
        sum += i
        print("the sum is: " + str(sum))
    list2 = range(1, num+1, 2)
    for j in list2:
        product *= j
        print("the product is: " + str(product))
    avg = (product + sum) / 2
    print("the average is: " + str(avg))
    diff = product - sum
    print("the difference is: " + str(diff))
    div = diff / avg
    return div

print(who_knows_why(number))
