end_num = int(input("Enter a number: "))
def sum_evens(to_num):
    # Step 2: Get data
    numbers = range(0, to_num + 1, 2)
    # Step 1: Initialize accumulator
    sum = 0  # Start out with nothing
    # Step 3: Loop through the data
    for number in numbers:
        # Step 4: Accumulate
        sum = sum + number
    return sum
# Step 5: Process result
print(sum_evens(end_num))

