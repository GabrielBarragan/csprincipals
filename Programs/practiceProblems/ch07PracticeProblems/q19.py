startNum = int(input("Enter a starting number: "))
endNum = int(input("Enter an ending number: "))

def sumAndProductAverage(sN, eN):
    sum = 0
    product = 1
    list = range(sN, eN + 1)
    for i in list:
        sum = sum + i
    print(f"Sum equals {sum}.")
    for j in list:
        product = product * j
    print(f"Product equals {product}.")
    avg = (sum + product) / 2
    return avg
    
print(f"The average of the sum and product is {sumAndProductAverage(startNum, endNum)}.")

