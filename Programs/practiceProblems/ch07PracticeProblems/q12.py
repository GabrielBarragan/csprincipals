num = int(input("Enter a number: "))

def factorial(n):
    fact = n                      # Set fact to passed value
    for i in range(n - 1, 1, -1):    # Count down from n to 1
        fact = fact * i            # Accumulate the product
    return fact

print(factorial(num))                                 # Call and print to test

