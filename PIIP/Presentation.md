# Personally Identifiable Information

## Definition
Personally Identifiable Identifiable or PII, is any information that can identify an individual. 

## A More Thorough Explanation
This can be any information that can identify you, from your name, to your phone number, to your adress. This can be used by pairing any two pieces of information to succesfuly identify you. 

## What is considered PII
There is no one thing that defines what PII is versus what is not PII. Any piece of information could be considered PII, however, just a full name is not enough to identify someone, while a social security number can. For PII to be effective, there has to be enough information that you can identify a specific individual.

## Sensitve Vs. Non-Sensitive PII
Though  it is your personal information, it is important to know what is ensitve and what isn't so sensitive, so you can still protect your information.

### Sensitive Information
Some sensitive information would be:

* Full name
* Social Security Number
* Driver's License info
* Address
* Credit Card info
* Passport info
* Financial info
* Medical records
* Passwords
* Other important things

### Non-Sensitive Information
Some not-so-sensitive information would be:

* Zip code
* Race
* Gender
* Place and date of birth
* Religion

Though this information wouldn't be that important, it could still be harmful if in the wrong hands.

## Brieching and Protecting PII

## Obtaining PII
Many companies obtain your personal information and store it. This might be used for targeted ads, to identify you while signing in, and to better personalize your experience on the website. 

## Stealing PII
Though companies take you information, other third parties want your PII too. Some ways that they get this information are: phishing and social engineering makes people reveal valuable information.

## Protecting PII
Some ways to make it harder to steal your information are: encrypt your data, using different, dificult passwords on each different site, also if you sell or donate a computer reformat your hard drive.

# Closing
PII is very important to protect, and though not all of it might be that important, you still need to protect it. 
