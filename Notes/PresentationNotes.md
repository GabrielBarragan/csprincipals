# Notes

## Group A
Remember the big ideas.
If you don't know  a question, do it later, it is better not do it than waste time an dget it wrong.
Quite easy to pass, quite hard to be perfect.
#### Good Resources
1. Khan Academy
2. College Board
3. Test Guide

## 3 Questions:

1. Every night at 12:00 am, you notice that some files are being uploaded from your PC device to an unfamiliar website without you knowledge. You check the file and find that these files are from the day's work at the office. What is this an example of?

- [ ] Network Worm.
- [ ] Keylogger Attack.
- [x] Computer Virus.
- [ ] Phishing Attack.

2. An organization requires multi-factor authentication before giving the site access to its employees. One of the authentication factors requires an employee to use a security token that is texted to their mobile phones.

What factor is being used to identify the employee?

- [ ] Inherence
- [ ] Knowledge
- [ ] Facial Features
- [x] Possesion

3. The same task is performed through sequential and parallel computing models. The time taken by the sequential setup was 30 seconds, while it took the parallel model only 10 seconds.

What will be the speedup parameter for working with the parallel model from now on?

- [x] 0.333
- [ ] 3
- [ ] 300
- [ ] 20


I got these questions from [Test Guide](https://www.test-guide.com/ap-computer-science-principles-practice-exam.html).
