# Repeating Steps With Strings

## Using Repitition with Strings
Python has abilities to use words or strings like numbers. A Python for loop can loop through letters, and + appends strings. 
```
    # Step 1: Initialize accumulator
    new_string = ""

    # Step 2: Get data
    phrase = "Rubber baby buggy bumpers."

    # Step 3: Loop through the data
    for letter in phrase:
        # Step 4: Accumulate
        new_string = new_string + letter

    # Step 5: Process result
    print(new_string)
```
## Reversing text
Write specifically, because you can accidentally make text go backwards. This code shows how.
```
# Step 1: Initialize accumulators
new_string_a = ""
new_string_b = ""

# Step 2: Get data
phrase = "Happy Birthday!"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string_a = letter + new_string_a
    new_string_b = new_string_b + letter

# Step 5: Process result
print("Here's the result of using letter + new_string_a:")
print(new_string_a)
print("Here's the result of using new_string_b + letter:")
print(new_string_b)
```

## Mirroring text
If we add letters to both sides of the string, it will act sort of like a mirror.
```
# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "This is a test"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = letter + new_string + letter

# Step 5: Process result
print(new_string)
```
Your accumulator variable doesn't need to be empty, if you set it to something, it will just show up in the middle of the new string.

## Modifying Text
You can loop through strings and modify them using find and slice.


## Chapter 9 - Summary
* __Accumulator Pattern__ - A set of steps that processes a list of values.
* __Palindrome__ - A word tht read the same forwards and backwards.
* __String__ - A group of letters, numbers, and other characters inside a pair of single, double, or triple quotes.

## Summary of Python Keywords and Functions
* __def__ - Defines a procedure or function in Python.
* __for__ - One kind of loop that repeats a certain amount of times.
* __print__ - Prints something onscreen. 
* __range__ - Returns a list of values. Can have up to three parameters.
* __while__ - A loop that loops until its logical expression is false.


