# Chapter 3 Names for Numbers

## Assigning a Name
A computer can associate a name with a value. To do this it creates a variable, a space in memeory that represents a value. A score in a video game or a name for a phone number are examples. These can change, as variables change, or vary. Computer memory is just numbers, so everything in memory is translated into numbers. In programming languages, giving a variable a value is called an assingment. Stating that x = 4 is saying that x is assigned to a space where the value is 4. After being stated to be 4, x can still be changed. 

### Legal Variable Names
*  Must start with letter or an underscore
* Can have numbers, just not as the first character
* Can't be a Python keyword
* Case sensitive, if something is uppercase in the first instance it is uppercase in all instances
* No spaces allowed. So to use "multiple words" write in camel-casing or with underscores between words.

##  Expressions

The right side of an assignment doesn't always need to be a value, it can also be an arithmetic expression. There are many different types of integer expressions, including two different kinds of division. The expression are:

### Integer Division

Integer division is just normal division. If you only want whole numbers use // instead of just one /. To learn more got to [Python code style guide](https://peps.python.org/pep-0008/)


### Modulo Division
Modulo divion is just normally dividing except you are solving for the remainder.  If you do x modulo y and x is smaller than y it will always print x. To do modulo use the percent (%) sign.

The rest of the expressions are what you would usually type, except for multiplication which is * .

## How Expressions are Evaluated
It goes down from parentheses, to negation (-x), to multiplication, division, and modulo, to addition and subtraction.



## Walking Through Assignment More Generally

Saying a variable equals another variable is just saying that the new variable equals the first variable's value. Variables can change, and the variable value that is above the assignment of the new variable is the one that the new variable takes. Printing the values is a good way to make sure the variables are being set to the right values. 

## Chapter 3 Summary
* Arithmatic Expression - contains a matheatical operator
* Assignment - Setting a variable's value
* Assignment Dyslexia - putting variable and value on the wrong sides
* Integer Division - Regular division. Other languages might only give you an integer, but Pythin 3 can give you decimals. To get an integer for a result use //.
* Modulo - Dividing for the remainder.
* Tracing - Keeping track of variables and how those variables change as statements are executed.
* Variable - Name associated with computer memory that can hold a value. Can change or vary.


