# Computers can Repeat Steps

## Repeating Steps
Often times you have to repeat steps in a process. Like stirring a bowl. The thing is though, doing things a lot can be tiring and boring, so you would get a machine to do it. Though you can get tired, computers can't. It can do the same thing until it loses power. A way to make a computer to do the same thing over and over is by making a loop (AKA iteration). 

## Repeating with Numbers
A for loop is away to repeat a or multiple statements in a program. A for loop will use a variable and make that variable take on the values for each of the numbers in a list one at a time. A list holds values in an order. Loops need to have a colon at the end of that first line of the loop,  and each line after that, the *body* of the loop must be indented. The body of the loop will be repeated for the assigned amount of times.

## What is a List
A list holds items in order. A list in Python is enclosed by brackets and hae commas between each value in the list. A list has an order and each item has a position on the list. Remember that the variables you use should be readable. Even though the computer doesn't care, other people do.

## The Range Function
The range function is used to loop over a sequence of numbers. If called with one integer it will loop through it and start from zero and end with one less than the assigned number. For example: if it is in range of 4 and you are to print the value of the loop it will print every integer from zero to one less than four. Therefore it will print from zero to three. If two integers are passes with a comma between them, it will print all the integers from the first number to one less than the second.

## There's a Pattern Here!
These programs have a pattern, one that is common when processing data. This is called an **Acumulator Pattern**. There are five steps to this pattern.
1. Set the accumultor variable to its first value. This is the value we want when no data is to be processed.
2. Get the data to be processed.
3. Use all the data using a for loop so that the variable takes on each value of the data.
4. Add each peice of the data into the accumulator.
5. Use the result for something.

### Using the Accumulator Pattern
The range function has multiple versions that are usable. By giving three numbers we can say the start, the end, and how much is being changed between each value.

## Adding Print Statements
This part is to develop a model of how the program workd. Are you able to look at a program and know what is going to happen? Can figure out the variable values? Remember to make a prediction about the value of the variable, then insert some print functions to dipay those variable values, and then prove if you're right or not.

## Chapter 7 Summary
Chapter 7 had the following concepts:
* Accumulator Pattern - A set of steps that processes a list of values.
* Body of a Loop - The statement(s) that are to be repeated in a loop.
* Indentation - Means that text has spaces at the begininning of the line. In Python it is used to show what statements are in the same block.
* Iteration - Ability to repeat a step or steps in a program. Also called looping.
* LIst - Holds a sequence of items in order.
* Loop - Tells the computer to repeat a statement or statements.

### Summary of Python Keywords and Functions
* def - Used to define a procedure or function in Python. The line with def must also end with a colon and every line in the body of that function must be indented 4 spaces.
* for - Statement in programming that says to repeat a statement or statemwnts a certain amount of times. One kind on loop.
* print - Will display the value of the items passed to it.
* range - Returns a list of consecutive values. If it has one parameter, it will return a list of numbers from zero to one less than the number entered. If given two numbers seperated by a comma, it will return all the numbers from the first number to one less than the second. If given three values, it will return the all the numbers between the first number entered and one less than the second number entered, but will do it by the distance you want. This is called **step**.
