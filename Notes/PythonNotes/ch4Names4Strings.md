# Chapter 4: Names for Strings

## Assigning a Name to a String

### Concatenating Strings
Computers can uses names to represent anything. We can name strings and do calculations with strings, just like how we did last chapter with numbers. A string is a sequenc of characters inside of single, double, or triple quotes. How do you calculate with strings? Well you use the addition symbol + to concatenate strings. Concatenating means to create a new string with the characters of the first string followed by the characters of the second. Also known as appending strings. Remember that spaces are not added automatically. To do that you must concatenate a string with just space in it. Remeber that if you start the string with a single quote end with a single quote, also use double quotes to end double quoted strings and triple quotes for triple quotes.

### Getting Strings from Users
We can use the input function (just write input) to get strings to obtain strings from uses. A value returned by input is always a string. Even if it looks like another data type. 

### Concatenating Strings and Numbers
We can print both strings and numbers, you can concatenate two strings, but if you try to concatenate a string and another value it will give you an error. A string and an integer look different to a computer, so to get around an error you have to use the str(num) function to convert a number into a string. Remeber that printing something in quotes it will print exactly what you wrote, while printing it without quotes will print the value assigned to it.

## Strings are Objects
Strings are objects in Python. This means that there is a set of functions that are built-in that we can use to manipulate strings. We  use dot-notation to invoke the said functions. These are lower() which returns a string with all the characters in lowercase, capitalize() which returns a string with the first letter of the string capitalized.

### Getting Part of a String
A string is a sequence of characters. Each character can be found by its index, which is a number between 0 and one less than te length of the string. In other languages one character is a different data types than a string. This isn't true in Python. In Python a character is a string with a length of one.

### Finding a Small String in a Bigger One
The function string.find(part) takes a part of a string and returns the index of the first character of said part. If the part isn't found it returns -1. If there is multiple of the same word, there is an optional argument that tells it to start searching. A slice is a way to get a part of a string. It also uses square bracket like an index, but has two integer values with a colon between them. The first number is when the part starts and the second is when it ends. Ot will include all characters from the first index up to, but not including, the second index.

### Getting the length of a String
The len(string) function uses a string as input and returns the number of characters in said string (it *does* include spaces).

## Strings are Immutable
Though you can manipulate a string to create a new string the original is immutable, or that it doesn't change. While the srings themselves can't change, you can change the value of the variable. This gets rid of the old string and assigns it to a new value.

## Chapter 4 Summary

### Concepts Learned
* Append - You can put two strings together using the + symbol. Also known as concatenate.
* Capitalize - Capitalize function returns a new string with the first character capitalized.
* Concatenate - Using + to concatenate (AKA append) two strings. Will create new string with all the values of the first string followed by the second string.
* Function - Returns a value. Can also take input, but doesn't have to.
* Immutable - Means that it doesn't change. Strings are immutable in Python. When you call a function too edit a string, it actually makes a new one.
* Index - The number associated with the position of a character in a string. Python starts with 0.
* Input - Data fed into a computing process. Input function in Python can get information from users.
* Object - Can have behavior. Strings are objects.
* String - A sequence of characters. Can be specifeied in single, double, or triple quotes.
* Substring - Part of a string.

### Summary of String Functions
* append - Can add two strings using + plus symbol. AKA concatenate.
* find - Takes a string as input and returns the index of the of the first occurance of that string.
* len - Takes a string as input and returns the number of characters (including spaces) in it.
* lower - Returns a new string with only lowecase letters.
* input - Returns a string from the user. Can also print out a message to the user, known as a prompt.
* slice - Can get a substring using [start] or [start:end] which will start at the given starting position and end at one character before the specified end position.

