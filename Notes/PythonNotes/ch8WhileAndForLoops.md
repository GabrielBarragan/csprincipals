# Chapter 8 While and For Loops

## Loops - For and While

### Example For loop
We have used the for loop. It's the loop that loops through the body of the loop a known number of times. The **body of the loop** are the statements that directly dollow the statement and  are indented to the right of the it. 

### Introducing the While Loop
Another type of loop is th while loop. It will keep looping until the while statement is proved false. The body of the loop is also indented, just like with the for loop. A **logical expression** is an expression that is either true or false. A while statement is a logical expression. While loops are used when the amount of times that are to be looped isn't known. 

## Infinite Loops
It is easy to make a computer loop statements, to make it stop is another thing. If a while loop's statement is always true, we call that an **infinite loop**. This means it will loop forever, and will never end, unless forced to stop.

## Counting with a While Loop
Just like with a for loop, while loops can count. You just need to set a counter variable and make it start at whatever value you want, make a while statement that tells it to be under a certain number, and then, in the while loop, you increment the value.

### Side by Side Comparison of a For Loop an a While Loop
While you can use while loops to execute a certain amount of times, it is better to use a for loop. This is because for loops use less code and while loops can accidentally create infinite loops.

## Looping When We Don't Know When We'll Stop
While loops are generally used when we don't know how many times we will loop. The body will repeat while the statement is true. The logical expression will be evaluated just before the body of the loop is repeated. 

## Nested For Loops
Bodies of loops can contain anything, even other loops. 

## Chapter 8 - Summary
* __Body of a Loop__ - Statement(s) that are to be repeated in a loop. Body of a loop is indicated in Python with indentation. 
* __Counter__ - A variable used to count something in a program.
* __Increment__ - To increase the value of a variable by 1.
* __Infinite Loop__ - A loop that doesn't end.
* __Logical Expression__ - A statement that is either true or false.

## Summary of Python Keywords and Funtions in Chapter 8
* __def__ - Used to define a function or procedure in Python. The line that starts with def must also end with a colon.
* __for__ - A for loop is a kind of loop that is used to loop throught the body of the loop a certain amount of times.
* __print__ - Prints the value of the items passed to it.
* __range__ - Returns a list of consecutive values. Can have three parameters. If there is only one value passed, it will create a list of all integers from zero to one less than the value. If two parameters, the first parameter will become the first value in the list, and it will print the integers from that starting value to one less than the second parameter. If three parameters, it will do the same thing as with two parameters, except the third parameter makes it count by that number. 
* __while__ - A loop that repeats the body of the loop while the logical expression is true.
