# Names for Turtles

## Assign a Name to a Turtle
Names can be other stuff than just numbers and strings, they can also be turtles or "screens" (a place where a turtle can draw). Turtles and screens can also be called objects. Objects are things that do actions in programs. Objects can have attributes and behavior. An example for an attribute is a position and an example of a behavior is being able to go forward. 

### What does a left turn of 90 mean?
Asking a turtle to turn left makes the turtle turn left based on the direction is giong in right now. Turtles keep track of what direction they are facing. Turning of 90 means the turtle will turn a quarter of a full spin of the direction specified.

## Procedures and Functions
Functions return a value. The Screen() and Turtle() code both create objects and return them, so they are functions. There is also something called a procedure, which do something but don't return anything. The forward() and left() commands are procedures as they do something but don't return anything. Other Python books don't make a distinction between functions and prcedures.

## More Turtle Procedures and Functions

| Name | Input | Description |
| ----------- | -----------------| ----------------------|
|backward | amount | Moves the turtle backward by the specified amount |
| color | colorname | Sets the color for drawing. Use 'red', 'black', etc |
| forward | amount | Moves the turtle forward by the specified amount |
| goto | x,y | Moves the turtle to position x,y |
| left | angle | Turns th turtle counterclockwise by the specified angle |
|pendown | None	| Puts down the turtles tail so that it draws when it moves|
| penup	| None | Picks up the turtles tail so that it doesn’t draw when it moves | 
| pensize | width | Sets the width of the pen for drawing |
| right | angle | Turns the turtle clockwise by the specified angle |
| setheading | angle | Turns the turtle to face the given heading. East is 0, north is 90, west is 180, and south is 270. |
| Turtle | None	| Creates and returns a new turtle object |

Turtles draw in a space that is 320 by 320 oixels. The center of that space is 0,0. Remember to put the pen down after you picked it up.

## Single and Multiple Turtles
Using multiple turtles doesn't always work. Turtles don't have the same attributes, so one starts off from the center point even after the other turtle moved.
