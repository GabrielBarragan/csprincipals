# Intoduction to Networking

## Computer Communicate Differently
Unlike humans computers are taking to one another all the time, this can be in short, medium, or long messages. In the early days of computing, computers were connected with wires. The way they sent data was sending all the data and waiting for each piece to be recieve in order. These wires connected comuters at massive distances, and it was quite effective, but also quite expensive.

## Early Wide Area Store-and-Forward Networks
The way college kids in different parts of the world developed to communicate with one another was by sending a message to a close computer, which sent it to a computer close to it, and so on, until it reached its destination. This could take a long time depending if lots of people were doing this.

## Packets and Routers
The most important innovation in sending these messages was by sending them in small parts, called packets. This was invented in the 60s, but wasn't really used until the 80s because it needed more computing power and more sophisticated networking than there was at the time.As this packet approach increased in use thy statrted creating computers specifically for this use, know as IMPs. These were renamed routers as they route where the packets are to go. Because of routers it became simpler to connect multiple computers on the same netweork. When multiple computers were onnected together by wires in a "Local Area Network" (LAN), you would then connect a router to it so they could send data through the "Wide Area Network" (WAN).

## Addressing and Packets
In these store-and-forward networks it was very important to know the source and destination computers for each message. Each computer was given a name or umber called its address. Having both the source and destination addresses of the coputers would allow the computers to route them better and faster if more than one path was available. The addresses were still needed for packets, but also they needed to say which position the packet was in the message. 

# Putting it All Together
SO basically it is that we use specialized computers called routers to route packets of data along a path from a source to its destination. These packets will pass through many different routers while traveling from the source computer to its destination. Although packets are part of a larger message routers send packets each on their own way based on the source and destination addresses. Sometimes packets arrive out of order, a later packet arriving later than an earlier packet. The term "Internet" comes from "internetworking" which is connecting many networks together, Our computers connect to a local network and the Internet connects multilple local networks.
