# Big Idea 5: Impact of Computing

## Main ideas

* New technologies can have both beneficial and unintended harmful effects, including the presence of bias.
* Citizen scientists are able to participate in identification and problem solving.
* The digital divide keeps some people from participating in global and local issues and events.
* Licensing of people’s work and providing attribution are essential.
* We need to be aware of and protect information about ourselves online.
* Public key encryption and multifactor authentication are used to protect sensitive information.
* Malware is software designed to damage your files or capture your sensitive data, such as passwords or confidential information.

## Vocabulary

* __asymmetric ciphers__ - Used for encrypting, uses two keys, one for the encription and the other for the decription.
* __authentication__ - The action of verifying the identity of a user or process.
* __bias__ - Being in favor of one thing over another, not considered fair.
* __Certificate Authority (CA)__ - A trusted identity that gives certificates to authenticate stuff sent by web servers.
* __citizen science__ - When people helps in scientific research.
* __Creative Commons licensing__ - Lets people use your work, even commercially as long as you are credited for the original work.
* __crowdsourcing__ - The process of obtaining information or work on a project from a large amount of people, generally on the internet.
* __cybersecurity__ - Being protected or protecting against criminal or unauthorized use of your electronic data.
* __data mining__ - The process of using large databases to get new information.
* __decryption__ - Converting data back into its non-encrypted or original form.
* __digital divide__ - The divide between people have readily available access to computers and the internet and those who don't.
* __encryption__ - Converting information into a code to prevent unauthorized access.
* __intellectual property (IP)__ - Something that someone has rights to and may apply for things like copyright, trademark, patents, and other things.
* __keylogging__ - The use of a program to record every keystroke of a user, generally used to get paswords and other imformation.
* __malware__ - Software used to harm or gain access to a computer.
* __multifactor authentication__ - A way to secure data and applications by requiring a user to show to or more credentials to log them in.
* __open access__ - The unrestricted right to use something, particularly academic writing or research.
* __open source__ - Software that the original is freely available and can be redistributed and changed.
* __free software__ - Users can run, copy, distribute, change, and improve the software.
* __FOSS__ - __Free and open-source software__ refers to software that has both free and open-source software.
* __PII (personally identifiable information)__ - Any data that can identify someone.
* __phishing__ - Sending messages and pretending to be trustworthy to get people to give you their personal information.
* __plagiarism__ - Using other people's work and saying it is you own.
* __public key encryption__ - Encrypting using two different keys, one the public key, which anyone can use, and the other the private key. Data that is encrypted with the public key can only be decrypted with the private key.
* __rogue access point__ - A wireless access point that is connected to a network, but not allowed to.
* __targeted marketing__ - Personalizing online ads by obtaining data on an intended audience.
* __virus__ - A code that an copy itself and generally has a bad effect.
