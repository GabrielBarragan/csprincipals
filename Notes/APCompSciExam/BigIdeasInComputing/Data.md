# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- abstraction - Removing parts of a program that aren't important.
- analog data - Data that is represented physically.
- bias - Systems that discriinate systematically against a group or individual.
- binary number system - About the negative and positive capabilities of an exponent of a floating point number.
- bit - Smallest unit of data that a computer can store and process.
- byte - Eight bits.
- classifying data - Four types: strings, floating point numbers, integers, and booleans. 
- cleaning data - Process of fixing and/or removiiing corrupted, incorrect, badly formatted, duplicate, or incomplete data.
- digital data - Data with two states: on and off.
- filtering data - Choosing a part of your data set and using it for viewing or analysis.
- information - Stuff that has context for its reciever.
- lossless data compression - Data reconstruction without a loss of information.
- lossy data compression - Compression that uses approximations and losing data to reduce data size.
- metadata - Data that gives information for other data.
- overflow error - Error that happens when the data type used to store the data can't store it due to it being too large.
- patterns in data - Used to analyze data better.
- round-off or rounding error - Difference between a result between using rounding and using exact math.
- scalability - Ability for a process to be used for many different things.
