# Collaboration Diversity
Collaboration on projects with diverse team members representing a variety of backgrounds, experiences, and perspectives can develop better computing products.

![Collaboration](https://www.notifyvisitors.com/pb/wp-content/uploads/2021/05/What-is-Team-Collaboration-Definition-Benefits-and-Strategies-Bann-er-image.jpg)

## In relation to bias
Having a diverse team in computer science can help fight against AI bias. A study found that a computer program developed by a private contractor was more likely to rate black parole candidates as higher risk when compared to other options. 

![Statistics](https://static.dw.com/image/43827698_7.png)
This AI owes its biased opinions to its designers. This is because the majority of the team working on the AI was unintentionally feeding it biased data. If the team had a more diverse community, then there would be less biased data being fed to the AI. This means the final product will be a more effective product.

## In relation to education
![More Statistics](https://media.wired.com/photos/5ab9b5cb9ccf76090d775118/master/w_2217,h_1975,c_limit/2604WEB_college-enrollment-v3-fix.jpg)
This graph shows that there is not much diversity in computer science majors. This can be explained by the fact that in low income areas, the schools tend to not have a CS class, while in higher income areas, the schools almost always have a CS class. If there are more diverse groups who have access to CS classes, then that will make a more welcoming and equal CS community.

## In Conclusion
Having a diverse team can lead to far more effective products that take all potential users into account. It can also lead to less unintentional bias which is a big problem, it also gives us more prospectives so products can become more effective, less baised, and overall, better. 
