# Big Idea 4: Computing Systems and Networks

## Main ideas

* Built-in __redundancy__ enables the Internet to continue working when parts of it are not operational.
* Communication on the Internet is defined by protocol such as TCP/IP that determine how messages to send and receive data are formatted.
* Data sent through the Internet is broken into packets of equal size.
* The Internet is scalable, which means that new capacity can be quickly added to it as demand grows.
* The World Wide Web (WWW) is a system that uses the Internet to share web pages and data of all types.
* Computing systems can be in sequential, parallel, and distributed configurations as they process data.

## Vocabulary

* Bandwidth - The maimum amount of data transmitable over the internet in a specific amount of time
* Computing device - A device that can do computations like math and logic without humans
* Computing network - A group of computers that sare data and resources with one another
* Computing system - A system of at least one computer and software with common storage
* Data stream - A sequence of digitally encpded signals to give information
* Distributed computing system - A type of system where parts of a software system are spread among many different computers
* Fault-tolerant - The ability of a system or computer to operate while not being interrupted if at least one of its components fail
* Hypertext Transfer Protocol (HTTP) - Rules for sending files over the internet
* Hypertext Transfer Protocol Secure (HTTPS) - Compination of HTTP and SSL/TLS for more security, how to transfer encrypted data over a secure connection
* Internet Protocol (IP) address - A group of numbers that identifies a device on a network
* Packets - Small part of a larger data 
* Parallel computing system - Way of using multiple processes to solve a problem
* Protocols - Rules set to determine how data is supposed to be sent between different decives on the same network.
* Redundancy - Where a part is duplicated so if it breaks, then there is a backup
* Router - Device that connects two networks and allows packets through those two
