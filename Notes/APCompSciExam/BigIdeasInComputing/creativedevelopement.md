# Big Idea 1: Creative Development

## Main ideas

* Collaboration on projects with diverse team members representing a variety of backgrounds, experiences, and perspectives can develop a better computing product.
* A development process should be used to design, code, and implement software that includes using feedback, testing, and reflection.
* Software documentation should include the programs requirements, constraints, and purpose.
* Software should be adequately tested before it is released.
* Debugging is the process of finding and correcting errors in software.

## Vocabulary

* code segment - A part of the of an object file or part of a program's veirtual address that has executable instructions
* collaboration - Working with someone to achieve a goal.
* comments - A explanation written in the program by the programmer about what is happening in that part of the code. Makess it easier to understand what the code is meant to do.
* debugging - How you find and get rid of the errors in your program.
* event-driven programming - A way to program a software that does stuff that is determined by user stimuli.
* incremental development process - Breaking down the developement of a software into smaller portions.
* iterative development process - Similar to incremental developement, but the chunks are developed and tested multiple times.
* logic error - An error that makes the code behave strangely, but doesn't shut down the program.
* overflow error - An error that happens when the data type that is being used can't store the data, as the data is too large.
* program - A series of instructions used to control how a computer or machine works.
* program behavior - How data is organized physically.
* program input - Data that is given to the program.
* program output - Actions that are done by the program for the intended audience.
* prototype - A basic model of something, which later models are developed from.
* requirements - A thing that is needed or wanted.
* runtime error - An error that is caused by having everything syntatically correct, and onloy having an error during the running of the program.
* syntax error - When a programmer writes an incorrect line of code.
* testing - Process of running a program to find errors.
* user interface - Where the computer and the human interacts.

## Computing Innovation

A _computer artifact_ is anything created using a computer, including apps, games, images, videos, audio files, 3D-printed objects, and websites. _Computing innovations_ are innovations which include a computer program as a core part of its functionality. GPS and digital maps are examples of computing innovations that build upon the early non-computer innnovation of map making.

## Development Process

![Development Cycle](https://kirkpatrickprice.com/wp-content/uploads/2020/01/Best-Practices-for-a-Secure-Software-Development-Life-Cycle.jpg)

## Errors

Four types of programming errors need to be understood:

1. Syntax errors
2. Runtime errors
3. Logic errors
4. Overflow errors

Which type of error is this?
Syntax error
```
for i in range(10)
    print(i)
```

Which type of error is this?
Syntax error
```
 # Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total) 
```

Which type of error is this?
Runtime error
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```
