# How is the APCSP exam scored? How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4, or 3 on the exam? What is the Create Performance Task and how is it scored?
It depends. It isn't only the questions on the written exam that are the whole score, there is also the Create Performace Test, which if you get a zero on that, your 5 will become a 3. The questions are distributed like this:
* Single select multiple choice questions : 57
* Single select with reading passage about computing innovation: 5
* Multi select: 8
A good score on it is a 3, 4, or 5. A 3 means "qualified", a 4 means "well qualified", and a 5 means "extremely well qualified". The tests are curved to emulatehow other college courses are scored. Lets say you get a 6 out of 6 on the performance task, the minimum questions that you need to get right for a 3 is 32. For a 4 it is 51 right. For a 5 you need 60 right. Anything lower, a 2 or 1, which are failing. Over the course of 4 yearse the average amount of people who pass is 70% so not much to worry about. The Create Performance Task is a task where you write a program, create a video explaining said program, and answering questions about it. There are six criteria, each worth one of six points. They are:
* Describing the overall purpose and function of the program
* Showing how your program uses data abstraction
* Explaining how complexity is managed in the program
* Demonstrating how your program uses procedural abstraction
* Using proper implementation of algorithms
* Test your program for intended functionality

# On which of the 5 Big Ideas did I score best? On which do I need the most improvement?
I did the best on Big Idea 4, which only had four questions, so I will say Big Idea 1: Creative Development. I need the most improvement on Big Idea 3: Algorithms and Programming and Big Idea 2: Data.


# What online resources are available to help me prepare for the exam in each of the Big Idea areas?
* [Score calculator](https://www.albert.io/blog/ap-computer-science-principles-score-calculator)
* [Princeton Guide to the Test](https://www.princetonreview.com/college-advice/ap-computer-science-principles-exam#scoring)
* [Full description of course and test, has a ton of unnecessary reading](https://apcentral.collegeboard.org/media/pdf/ap-computer-science-principles-course-and-exam-description.pdf?course=ap-computer-science-principles)

