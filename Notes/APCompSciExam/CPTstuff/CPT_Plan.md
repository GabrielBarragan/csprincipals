# Plan
Evan and I are making an online battleship game. You will be able to say where to put ships on the board and where to shoot, just by typing in the positions. You will also be able to say which rooms you will be on by typing in the name. A host computer will host the games.

## 3a - What will your input and output be?

### Input
Our input will be done by simply typing in commands in the dedicated boxes. These boxes will ask which server to join, where to place your ships, and where to shoot. 

### Output
Our output will be text written onscreen. This can be in the form of the board, what server you are joining, if you hit another person's ship, if one of your ships got hit, etc.

## 3b - Where will you use a list?
We will use lists for many things, the values of hit or miss for each tile, what are acceptable values that can be written for where to place/shoot, who is in a server, and what servers are open. 

## 3c - Does your function have sequencing, selection, and iteration?

### Sequencing
Our functions will use sequencing by, for example, first taking you to the menu page, to then bringing you into a waiting page, to the game page.

### Selection
Our functions will have selevction through many if statements and while loops. Like if you say a place where the opponent's ship is, it will say that you hit a ship, and you will get to go again. If you don't hit an enemy ship, it will say that you missed.

### Iteration
Our functions will have iteration through the for and while loops. Some examples are: we will let you keep on shooting if you hit, or keep placing ships if you have some left.

## 3d - Do you call your functions more than once with different output depending on the input? 
Yes, like if you want to shoot at a place and enter the input a4, it could be a  hit, but if you said f9, it could be a miss
