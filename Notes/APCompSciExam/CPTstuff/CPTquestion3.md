Written Response (Question 3) for Create Performance Task

3a. Provide a written response that does all three of the following:

Describes the overall purpose of the program
The purpose of the program is to test how GASP is used to make a basic game.
Describes what functionality of the program is demonstrated in the video.
The video shows your and the robots' movements, collisions between you and a robot or between two robots, teleportation of the player, creation of "junk", collision between the robots and the junk, winning the game, and losing the game.
Describes the input and output of the program demonstrated in the video.
You input with either the eight keys around s, k, or by using the numpad. This moves your character in one of the eight cardinal directions. Another key also teleports you to a random place on screen.
3b. Provide a written response to the following:

Capture and paste two program code segments you developed during the administration of this task that contain a list (or other collection type) being used to manage complexity in your program.

The first program code segment must show how data have been stored in the list.
![Image of the code](/Downloads/Screen_Shot_2022-12-07_at_5.25.17_PM)
The second program code segment must show the data in the same list being used, such as creating new data from the existing data or accessing multiple elements in the list, as part of fulfilling the program’s purpose.
![Image of the code](/Downloads/Screen_Shot_2022-12-07_at_5.26.21_PM)
Then provide a written response that does all three of the following:

Identifies the name of the list being used in this response
The list is called robots.
Describes what the data contained in the list represent in your program
It represents all the robots.
Explains how the selected list manages complexity in your program code by explaining why your program code could not be written, or how it would be written differently, if you did not use the list
If a list wasn't used, it would have taken 3 variables per robot, which would take a lot of work.
3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the administration of this task that contain a student-developed procedure that implements an algorithm used in your program and a call to that procedure.

The first program code segment must be a student-developed procedure that:

1. Defines the procedure’s name and return type (if necessary)
2. Contains and uses one or more parameters that have an effect on the functionality of the procedure
3. Implements an algorithm that includes sequencing, selection, and iteration
![Image of move-player](/Downloads/Screen_Shot_2022-12-07_at_6.22.53_PM)
The second program code segment must show where your student-developed procedure is being called in your program.
![Image of calling the function](/Downloads/Screen_SHot_2022-12-07_at_6.25.10_PM)
Then provide a written response that does both of the following:

Describes in general what the identified procedure does and how it contributes to the overall functionality of the program
This function is the basis of how to move the player around the screen, it makes you able to interact in the game.
Explains in detailed steps how the algorithm implemented in the identified procedure works. Your explanation must be detailed enough for someone else to recreate it.
Move player starts with using all the variables in the player Class. Then it uses a variable named key that reacts whenever a key is pressed. Then it calls safelyplace-player to place you down. Then moves through if staments to check what key you pressed and changes your x and y by 1.
3d. Provide a written response that does all three of the following:

Describes two calls to the procedure identified in written response 3c. Each call must pass a different argument(s) that causes a different segment of code in the algorithm to execute.

First call:
You press the key x, which moves you down.
Second call:
You press j and you move left.
Describes what condition(s) is being tested by each call to the procedure

Condition(s) tested by the first call:
What key you pressed.
Condition(s) tested by the second call:
What key you pressed.
Identifies the result of each call

Result of the first call:
Moving down.
Result of the second call:
Moving left
