 Intoduction to Networking

## Chapter 1: Introduction

### Computer Communicate Differently
Unlike humans computers are taking to one another all the time, this can be in short, medium, or long messages. In the early days of computing, computers were connected with wires. The way they sent data was sending all the data and waiting for each piece to be recieve in order. These wires connected comuters at massive distances, and it was quite effective, but also quite expensive.

### Early Wide Area Store-and-Forward Networks
The way college kids in different parts of the world developed to communicate with one another was by sending a message to a close computer, which sent it to a computer close to it, and so on, until it reached its destination. This could take a long time depending if lots of people were doing this.

### Packets and Routers
The most important innovation in sending these messages was by sending them in small parts, called packets. This was invented in the 60s, but wasn't really used until the 80s because it needed more computing power and more sophisticated networking than there was at the time.As this packet approach increased in use thy statrted creating computers specifically for this use, know as IMPs. These were renamed routers as they route where the packets are to go. Because of routers it became simpler to connect multiple computers on the same netweork. When multiple computers were onnected together by wires in a "Local Area Network" (LAN), you would then connect a router to it so they could send data through the "Wide Area Network" (WAN).

### Addressing and Packets
In these store-and-forward networks it was very important to know the source and destination computers for each message. Each computer was given a name or umber called its address. Having both the source and destination addresses of the coputers would allow the computers to route them better and faster if more than one path was available. The addresses were still needed for packets, but also they needed to say which position the packet was in the message. 

### Putting it All Together
SO basically it is that we use specialized computers called routers to route packets of data along a path from a source to its destination. These packets will pass through many different routers while traveling from the source computer to its destination. Although packets are part of a larger message routers send packets each on their own way based on the source and destination addresses. Sometimes packets arrive out of order, a later packet arriving later than an earlier packet. The term "Internet" comes from "internetworking" which is connecting many networks together, Our computers connect to a local network and the Internet connects multilple local networks.


## Chapter 2: Network Architecture

To make something as complex as the internet, you first have to make it into smaller parts. To make the internet, it was devided into four subgroups:

1. Application
2. Transport
3. Internetwork
4. Link
These four are part of the model refered to as the "TCP/IP model". This refers to the Transport Control Protocol (TCP), which is used to implement the Transport layer and Internet Protocol (IP). It is visualized as a stack with the Application layer on top, an the Link layer on the bottom.

## The Link Layer
This layer is responsible for connecting your computer to the local network and for moving the data across that network in a single bound. Generally when using a wireless device, that device is only transmitting data a short distance away. The technology of this layer is often used by multiple computers at the same place. This layer has to solve to problems when dealing with these shared networks. The first of the probles is how to encode and send data over the link. For wireless it is deciding which frequencies to use and for wired it is which voltage to make the wire and how fast that wire should be able to transmit data. If using fiber optics they have to agree on what frequencies of light that should be used. The second problem is how to send the data with other computers trying to send the data at the same time. If all computers tried to share data at the same time, then their messages would collide and the places where the messages are being sent would only recieve noise. The way of using packets makes it easier. If using packets one computer will wait its turn while the others are sending their data and vice versa. The way it was designed to do this was by using the method called the "Carrier Sense Multiple Access with Collision Detection" (CSMA/CD). This is a simple concept where the computer listens to see if another computer is sending data, if there are none, then great! It will send the data. If there are, then it will wait. If two computers fail at transmitting as a collision occured, retry and the computers will wait a different length of time to reduce the cahnces of another collision. This mechanism works well when one computer wants to send data or when multiple are trying to. Some connections use things like SCMA/CD, like WiFi connection. Other layers like fiber optics aren't shared and are genrally used for connections between routers. The link layer is used for one small hop, so it only goes small distances, but for large distances we need many hops .

### The Inter Network Later (IP)

After the first link, the packets will go in a router. The packet has a source address and a destination address. This is used to fins ou the best way to move the packet to its destination.As there are so many packets, routers can't always choose the best paths, so it makes its best guess. Each of the other routers also do this.As the packet gets closer to the destination, the routers get a better idea of where the packets need to go. Sometimes though, issues can pop up when the packets are sent on its destination. Routers deal with this issue by communicating with one another so they can know which ways don't work well, or which are out of commision entirely, this makes the speed faster. Even with all this though, packets can still be lost.

### The Transport Layer (TCP)

The layer that deals with these lost packets is known as the TCP. Sometimes the packets are sent out of order and that's where the destination computer needs to come in. The destination computer sends back acknowlegment to the main computer as it is reconstructing the message. If parts of the message are missing, then the destination computer resquests for the source computer to resend the missing data. The sending computer must keep a copy of the parts of the message until the acknowledgement of the succesful deliverance of that part of the data. Only then can it delete the data and and some more. The amount of data that a computer sends before waiting for acknowlegment is known as the "window size". If it is too short, then it will be slow as it is always asking for acknowlegement. If it is too long, then it can cause traffic porblems by overloading routers or the like. It is solved by having the window size be small in the beginning and get bigger over time if the acknowlegment is sent in a timely fashion. If it isn't, then the window size will stay small. 

### The Application Layer

All the previous layers are working well, then the last thing to worry about is if applications can be built to use these network connections. In the first widely used Internet, the first networked applications allowed people to log into computers remotely, transfer files between computers, send email, adn to do real-time text chats between computers. In the 90s, the World Wide Web was developed and was focused on reading and editing documents wit images. Today the web is the most used network application used around the world. All the other older applications are also widely used. Application are genrally seperated into two halves: the "server" and the "client". The server runs on the destination computer and it waits for incoming networking connection. The client runs on the source computer. When using a web browser, you are using a "web client".  This makes connections between web servers and displaying the pages stored to those servers. The Uniform Resource Locators (URLs) that the browser shows are the web servers that the client is contacting to get the viewed documents. When developing the server and client half of the application, an "application protocol" must be defined. This states how the two halves will exchange messages over the network. The protocols are specific to the needs of each application.

### Stacking the Layers

The reason the four layers are depicted stacked is because each layer helps the layers above and below it achieve networked communications. The routers that do the moving of the packets don't know how the Transport and Applications work. They only understand the Internetwork and Link layers. Those two layers are all that's needed to move the packets across many links to the destination. The other two only come into play after the packets are delivered. If you wanted to make your own netorked application, you would probably only deal with the Transport layer and none of the others. They are important for the Transport layer, but you don't need to worry about the lower-layer detalils. The layer system makes it simple to write networed applications as many of the complex details can be ignored.

## Chapter 3: Link Layer

The lowest layer of the Internet Architecture is the link layer. It is called the lowest layer because it's the closest to physical networking. Most of the time the link layer sends data over fiber optics, wire, or a radio signal. A large part of the link layer is the fact that generally the data cannot transmit large distances. This is because the data only goes over one link. 

### Sharing the Air

When connecting to the Internet over WiFi your device is sending data and recieving dat with a weak radio. The computer can only send data a small distance, so it sends the data to the router. The router then sends you to the internet. All computers nearby the router with their radios turned on will recieve the packets the base station transmits, no matter where the packets are supposed to go. Therefore computers need something to see which packets are theirs and which packets are for others. Because of this, some computers could listen in on and capture your and other people's packets. Generally every device is given a its own serial number. The router also has its own serial number. These are generally in the for of a 48-bit serial number. These are called the "Media Access Control" or "MAC" address. This is the to or from address of a postcard. Every time you are connected to a new Internet, you are asking who is in charge of the Internet. If a gateway is on the network, then it will send the message back to the computer. Once connected to the network, it can then use the network's serial number as the address. 

### Courtesy and Coordination

The way computers coordinate is by two ways: the first is "Carrier Sense" this is done by listening for a transmission and waiting until it is done to send your message. The second is Collision Detection, where the computer expects that both of the messages collided and stops transmitting. The way they coordinate this is then they both have different wait times, which causes them to resend them correctly. 

### Coordination in Other Link Layers

When the link layer has lots of transmitting stations, then each computer is given a token for when they are allowed to transmit. They can't transmit until they have the righ token. Instead of waiting for silence, they have to wait for their turn. When given the token and having something to send, they send, once sent, they give up the token and waits until the token comes back. The "try then retry" approach works well when there are few computers, but on a token-style network, and there isn't much data being sent, then you still have to wait until the token comes back. The token approach works when there is a lot of traffic. 

### Summary

The big benefit of the layered architecture is that those that make it don't have to work on the more complicated stuff above that layer.

## Internetworking Layer (IP)

To move data across the world, it needs to make many hop across many networks. 

### Internet Protocol (IP) Addresses

We can't use link layer addresses when hopping route packets over many routers. This is because there is no relation between the link layer address and the location where it is connected to. Because of this the address that is assigned is made according to what location you are at. There are two types of IPs. The old (classic) IPv4 and the new IPv6. IPV4 consists of four numbers seprated by dots. Each of these four numbers can be from 0 to 255. Because of the shear amount of computers we have nowadays we are running out of IPv4 addresses to assign to them. Therefore we switched to IPv6. The most important part of an IP is that it can be split into two parts. One of these is the "Network Number". The second is the "Host Identifier". This first number is the same for every computer on one specific Internet connection. Because of this approach routers don't need to keep track of billions of computers, they only need to keep track of a million or less network numbers. 

### How Routers Determine the Routes

Though having these Network Numbers makes the amount of endpoints a router needs to think about way smaller, they still need to think about how to route the packets to that network. New routers don't know all the routes. When finding a packet that needs to go a route a way it doesn't know, it simply asks its neighbors how to route it. They tell it how to and it routes it. A new router creates a map of netwro numbers to other links so it can route packets well. This is called the "routing table". Generally each router has a quite complete routing table and it is quite rare to encounter a new network number. But if this does happen, it learns to route the first packet and then remembers for all the other packets.

### When Things Get Worse and Better

Sometimes there are problems on the network and the router has to find a way around those. To fix this routers simply discard all data being routed to that and that part of teh routing table, then proceeds to ask its neighbors for routes that don't go that way. During this time packets are routed slower as the routing tables are rebuilt to the new configuration. That's why it is important to have two different routes. If there are, then that netwpork is called a "two-connected network". These can recover from any single link outage. Where there are many network connection, the network could lose many links without ever being completely disconeecgted. But if at home or school and you have one connection and that cionnection is down, then you are down compkletely. If the link is fixed, the n the crouter will try to use these new links to the best of its ability. All the time these routers are trying to makje their routing tables better. 

### Determining Your Route

Nowhere on te Internet knows what way your packets will take to a particular destination. Even the routers don't know the whole way. They only know the next link in line. Even with all of this there is a way to trace your route from your computer to a destination computer. This is called "traceroute" (or on some OSs tracert). Even though we have this it will only be a "pretty good guess" as not all the packets go the same way. This is done by using the way to unstick packets. This is called Time to Live (TTL). This is done by starting with a number of 30 and subtracting that by one for each link that is crossed. This stops the loops by if it fails the TTL test, them the packet is thrown away. Baxk to the way trceroute works. This is done by sending packets in a tricky manner so that the routers that packerts go through give it a notification. 

### Getting an IP Address

Computers need a new IP address to correspond to whatever network it is connecte to. The way it does this is by using something called tje "Dynamic Host Configuration Protocol" (DHCP). It is quite simple. After asking the question used on the Link layer, the computer asks for a gateway to the INternet and asks for the networks IP and which IP that computer should use. 

### A Different Kind of Address Reuse

Many addresses start with 192.168 and 10. 192.168 means it is a "non-routable" address. This means they won't be used as real addressers that can route data across the core of the network. They can be used ona single local network, but not the globl one. The way this happens is that it is doing something call "Network Address Translation" or "NAT". It has multiple workstations that are sharing the same IP adrress. The computer uses the address while routing the packets, but the address is eventually cahnged over time. 

### Global IP Address Allocation

If you want to connect to the internt for a new group, you would need to contact an Internet Service Provider. The ISP would give you a range of IPs. These could be given to coputers on your network. The top level of IP allocations are five Regional Internet Registries (RIRs). Each of these allocates IPs for a large geographic area. The five are: North America (ARIN), South and Central America (LACNIC), Europe (NCC), Asia-Pacific (APNIC) and Africa (AFRNIC). Before we used IPv4 addresses. Now we upgraded to IPv6. These are 128-bit addresses.

## Chapter 5: The Domain Name System

The Domain Name System lets you access sites by their domain name. This makes it so you don't need to remember numbered IP addressses. Adding the step of of looking up an IP for a DNS address makes it eaasier to move a server from one place gto another. The server is given a neew IP address and the entry form the domain adrress is updated. As users never see thwe IP, a server can be moved to a new network withou affecting the user;s ability to access the server.

### Allocating Domain Names

Unlike IP addresses which are based on where you connecgt to a network,  domain names are allocated based on who "owns" the domain name. At the top of this hierarchy is the organizartion called the Internationak Corporation for Assigned Network Names and Number (ICANN). They choose the top-level domains (TLDs) and assign those to other organizati0ns to manage. They also assign two-letter country code top-level domain names like .us to countries around the world. These are call Country-Code Top Level Domain Names (ccTLDs). Countries also add second level TLDs. Policies for applying for domain namkes with any ccTLD can vary widely from one country to another. Once a domain name is given to an organizartion, that organization can add subdomains within thta domaim. 

### Reading Domain Names

When looking art the IP address, the left prefix is the "Network Number".  So we read IPs from left to right, from least specific to most specific. For domain names we read from right to left. 


## Chapter 6: Transprt Layer

The thing with the internetwork layer is that it doesn't guarantee the delivery of every packet, because of this, some are lost or misrouted. The thing is people want to send whole files or messages over the internet *reliably*. For it to work all the packets need to be assembled in the right orderto reconstruct it on the recieving system. It also has to deal with packets that come out of order or don't come at all. The Transport layer is where the reliability and reconstruction happens. Like thr IP layer, the Transport layer adds data to solve the issue of packet reconstruction.

### Packet Headers

If looking at a packet going across a link, you would see three headers: a link, IP, and Transport Control Protocol (TCP) header. The link header is removed when arriving to the next router, then the packet is given a new one after. The IP and TCP headers, though, stay with the packet the whole journey. The IP header holds both the source's and destination's IP addresses as well with the TTL of the packet. It is sent in the source and is unchanged as it moves through the various routers on the journey. TCP headers show where the data in the packets belong. When it is seperated into packets it knows which position each packet is in, adding an offset that is in every packet.

### Packet Reassembly and Retransmission

When the destination computer gets the packets it looks at the offset position so it can put the packets in order. Just by checking if the packets are in the right order is how the Transport layer deals with the ones that are out of order. If it gets one out of order, it holds it in buffer, waiting to put it in until the previous ones are put in place. To no overwhekm the layer, packets are only sent once per period of time. This pause is called the "window size". The sender keeps track how long it takes to get the acknowlegments, and if they come slowly then it sends less data, if quickly, then it sends more. If one is lost, it doesn't revcieve the acknowledgement, so it waits to send new packets. At soe point the recieving computer sends a packet to the sender saying where the missing packet is supposed to be and the sender backs up the data and resends it. Tgsis is only from the last succesful packet though, so no need to send the same packets twice. THus creates a simple way to send large messages relably. When streming data it continuoulsy checks for acknowlegdements anf adjusts the window size in accordance. 

### The Transport Layer in Operation

One of the important parts is that the sending computer needs to store the data until it has been acknowledged. Once recieved, the sending computer can get riid of the sent data. 

### Application Clients and Servers

Tge whole point of the Transport layer is to give reliable connecton betweent the applications so they can send and recieve long streams of data. FOr the application it asks the Transport layer to connect it to an application on a remote host. The computer that stars the connection is called the "client" and the computer that responds is called the "server". The combination is called the "Client/server" application because both parts must work together. 

### Server Applications and Ports

When the client wants to make a connection to the server it's importnt that it goes to the right server on that computer. One of these computers can have many servers at one time. We use something called "ports" to allow the client to choose which server to interact with. When a server starts up, it "listens" for incoming connections. Once registered that it's ready to recieve connection, it waits until the first one is made.

## Chapter 7: The Application Layer

This layer is where the networked software like web browsers and video players operate. We interact with the applications and those applications interact with the network. 

### Client and Server Applications

Remember that there are two parts to have a networked app to work. These are the client and the server. The server runs somewhere on the internet and has the information that the users want to view of interact with. The client makes connections to the sercer retrieves the information, and shows it to the user. These apps use the Transport layer to exchange the data of the computers. To browse a web address you have to have a web application runnning on your computer. When you type the address it sends you to the server, gets the pages so you can biew them, and then shows you them. The browser requests to connect to the server, your computer then looks up the domain name to find their IP, and then makes a connection to the server, later requesting data from that server. When data is recieved the web browser shows it to you. The other end of the connection is the "web server". This is always up and waiting for incoming connections. 

### Application Layer Protocols

The name of the protocol between web clients and web servers is the "HyperText Transport Protocol" (HTTP). If you see this at the beginning of a website name, then it says you are trying to recieve a document using HTTP. One reson that HTTP is so succusful is the fact that it is quite simple compared to other client/server protocols. 

### Exploring the HTTP Protocol

The "telnet" was developed in 1968 and was made according to one of the earliest internet standards. The INternet was made in 1985 by the NSFNet project. And its precurser, ARPNEY was made in 1969. Telnet made made befoe even the first TCP/IP network was made. You can still access Telnet! To do this you have to use the Terminal is a Mac or on Linux. It is also in the OSs form Windows 95 to WIndows XP, but not in the later versions. The headers of the document show the document's metadata.

### The IMAP Protocol for Retrieving Mail

HTTP is one of the many client/server application protocls on the INternet. Another common one is used so you can get mail from a central server. As your computer might not be on at all times, when the mail is sent to you, it is sent to a server and stored there until you turn on your computer and retrieve any new mail. 

### Flow Control

NOw that we are looking at the Application layer, we can see the applications that are the source and destination of the data. 

### Writing Networked Applications

MAny of these apps are written in one or more programming languages. Mqany of these have libraries to write application code to send and recieve data across a network. 

## Chapter 8: The Secure Transport Layer

In the early Internet, netwroks were small and routers were in safe places. As long as the users protected themselves from unwanted connection, then there was no need to protect the data from people while it was crossing the network. Because of this, the Link, Internetwork, and Transport layers were focused of efficiency rather than privacy. The are two ways to secure network activity. The first is to make sure that all the network hardware is in physically secure places so someone can't sneak in and monitor the traffic while it is crossing the Internet. The other way is to encrypt data in your computer before it is sent over the first link, and then decrypt when it is recieved.

### Encrypting and Decrypting Data

Encrypting means transforming plain text into a "ciphertext" and decrypting is doin the opposite. Generally all encrypting needs to have a secret key thatb both the sender and reciever know of so they can decrypt the data.

### Two Kinds of Secrets

In the early days of the Internet, encryption would be done by sending the encrypted email and then the sender callin gthe reciever to tell them how to decrypt it. The new way to encrypt was made in the 1970s, with the idea of the asymmetric key was developed. The idea of this is that one key is used to encrypt and another is used to decrypt it. The computer that recieves the data chooses both the encrypted and decrypted keys. The sender gets the encryption one and the reciever gets the decryption one. The encrytpion key is widely called the "public" key as it can be widely shared. The decrytion key is known as the "private" key as it never leaves the computer that created it. This is used as if an attacker had the public key and the encrypted message, then they couldn't decrypt it. 

### Secure Sockets Layer (SSL)

The way network engineers added security without breaking any of the other layers was by making an optional partial layer between the Transport and Application layers. This was called the Secure Sockets Layer (SSL) or Transport Layer Security (TLS). When the applciation reqested to make a connection to a local host, it could ask for either encrypted or unencrypted. If encrypted, then the Trnasport layer would encrypt the packets before sending. 

### Encrypting Web Browser Traffic.

We know that it is using the TLS instead of the unencrypted Transport layer if the URL has https instead of http. The browser will also generally show a lock in the address bar if it is encrypted. Because https is a little moe expensie it was generally only used for pages with passwords and other sensitive data. Nowadays though, all sites are being moved to https. 

### Certificates and Certificate Authorities

One issue with the public/private key is knowing if the key you got was really from who it ays its from. Because of this the Certificate Authority (CA) was created. When a browserr is initially knows a bunch of well-known CAs. If given a public key given by these well known CAs, it trusts it and uses it to encrypt and send your data. If given one not trusted, then it will warn you before sending your data using the key. 
