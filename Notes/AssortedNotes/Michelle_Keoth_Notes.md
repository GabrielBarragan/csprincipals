# Michelle Keoth

## Edge Computing
Edge computing is computing at the source of the data. This is usually done with a microcontroller. A microcontroller is a small computing device without an OS. This is different from a microcomputer, which *does* have an OS. An example of a microcontroller is an Arduino UNO, an example of a microcomputer is a Rasberry Pi. Using these devices, the data is collected directly, making the data more accurate from where you are, instead of having data of something near you. 

## Machine Learning
Machine learning is a way to create AI. It does this by trainging a computer to recognize things using large datasets. An example in her presentation was the use of Edge Impulse to find out what kind of bird was shown. The way she used it, with microcontrollers to find the data, is called TinyML. TinyML is the use of small devices like microcontrollers and microcomputers to create machine learning.

## Edge Impulse
Edge Impulse is a software that uses Python to create machine learning models. We are able to use it for free using a student account. It is also compatible with the Arduino IDE. Ms. Keoth used Edge Impuse to create her bird watching program. 
