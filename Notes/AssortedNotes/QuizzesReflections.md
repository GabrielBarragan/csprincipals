## Note: I will do two questions for Quiz 3 and one question for 4 as it is uneven and there is no integer half of three

# Quiz 1

## Question 9:
Musicians reocrd their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?

- [ ] The file was moved from the original location, causing some imformation to be lost.
- [ ] The recording was done through the lossless compression technique.
- [ ] The song file was saved with higher bits per second.
- [ ] The song file was saved with lower bits per second.

- My Answer: a. 
- Reasoning: I thought that the last two answers meant that they were saved slower or faster, and I knew b was wrong.

- Real Answer: d.
- Reasoning: The file was saved with each second of sound having less bits transfered, AKA it was lossy.

# Quiz 2

## Question 10:
Using a linear search, how many iterations would it take to find the letter x?
``` str <- [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]```

- [ ] 3
- [ ] 4
- [ ] 23
- [ ] 24

- My Answer: c.
- Reasoning: I had no idea how searches worked, but I thought it was counting so I probably got 23 by starting from 0.

- Real Answer: d.
- Reasoning: Starts with first and counts down the list.

# Quiz 3

## Question 1:
Consider this sequence - 1010001011000000110110 - how many bytes is this?

- [ ] 1 byte
- [ ] 2 bytes
- [ ] 3 bytes
- [ ] 4 bytes

- My Answer: d.
- Reasoning: I forgot how many bits were in a byte.

- Real Answer: c.
- Reasoning: There are 8 bits per byte.

## Question 2:
How many its are in 4 bytes?`

- [ ] 16
- [ ] 32
- [ ] 64
- [ ] 128

- My Answer: d.
- Reasoning: I forgot how many bits were in a byte.

- Real Answer: b.
- Reasoning: There are 8 bits per byte.

# Quiz 4:

Question 10:
If your algorith needs to search through a list of unsorted words, what type of search would you use?

- [ ] Linear Search
- [ ] Binary Search
- [ ] Bubble Sort
- [ ] Insertion Sort

- My Answer: d.
- Reasoning: I had no idea how searches or sorts worked so I just guessed.

- Real Answer: a.
- Reasoning: As the list is unsorted the computer can only go down the line until it finds a word.
